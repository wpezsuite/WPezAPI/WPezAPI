## WPezAPI

__A collection of WP classes and functions remixed The ezWay.__

Note: The below is focused on the Get components (as nothing else has been addressed yet).


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

There are a couple essential things to know about how this (i.e., Get) works (and why it's so wonderful):

1) Many frequently used functions (i.e., the value they return) are now accessible as object properties (via a magic __get() or dedicated public method). 

   For example, get_permalink() becomes $new_post->url or $new_post->getURL(),_with $new_post being an instance of the WPezAPI\Get\Post object_.

2) Traditional WordPress classes and functions are no longer "ordered randomly." Instead there's a logical and more natural hierarchy. 

   For example, the Post object has a property ->author. It's an Author object, and that Author class extends our User class. The result? Now you retrieve the post author's display name with: $new_post->Author->name_display or $new_post->Author->display_name. No need to remember functions, now you simply get what you want.

3) What's even better is...when appropriate, property values are added to an object "on demand." That is, for example, the get_permalink() function isn't run until you request the ->url (property), or the Post's Image object won't be instantiated until you actually request an Image property. We keep it as clean & lean as possible.  

4) That said, when you start with an instance of WPezAPI\Get\Post it will (automatically) instantiate any subsequent "children" (e.g., Taxonomies) as needed. You don't have to worry about "manually" instantiating anything further, and nothing will be created until you actually need it. Again, more doing less actually thinking and coding.

5) It is strongly recommended you dip into the code of each class to get a better sense of what's available to you (and what's still a TODO). Don't panic, it's all very obvious in a why didn't I think of that sort of way.

6) Please also, see the Examples section below.


### SETUP

1) Within your mu-plugins folder (Ref: https://codex.wordpress.org/Must_Use_Plugins) add a (child) folder WPezSuite.

2) Then situate WPezAPI within the WPezSuite folder. 

3) Add a file to mu-plugins that requires (or includes) WPezAPI/Get's wpez-get.php, the file that boots up the autoloader.

4) Now instantiate...

$new = new \WPezSuite\WPezAPI\Get\Post\ClassPost( "some post ID" );

5) That's it! You're ready to rock & roll The ezWay.


### (SOME) EXAMPLES
*Note: Properties are __not__ case sensitive.*

__> Post title e.g., $post->post_title__

$new->title


__> Post Meta (all)__

$new->Meta


__> Post Meta with a key of 'some_meta_key'__

$new->Meta->some_meta_key 

*- Note: For the Meta class, by default, the __get() is single = true. Also, if your meta keys use - (hyphen) instead of _ (underscore) the magic get isn't going to work. But you can do this...*


__> Post Meta with a key of 'some-meta-key' (keys with hyphens not underscores)__

$new->Meta->getSingle('some-meta-key')


__> Post Meta with a key of 'some_other_meta_key' where there are multiple (meta) rows for this key__

$new->Meta->getMulti('some_other_meta_key');


__> Post Author > Meta (all)__

$new->Author->Meta


__> Post Author > Meta > last_name__

$new->Author->Meta->last_name


__> Post Author > Gravatar > img - returns a ClassImg obj__

$new->Author->Gravatar->img



__> Post Taxonomies__

$new->Taxonomies 

*- Will return an instance of the Taxonomies object.*

*- Note: This is not all the registered taxonomies, only those attached to this particular post.*


__> Post Taxonomies > the taxonomy: post_tag__

$new->Taxonomies->post_tag

*Will return an instance of the Taxonomy object for post_tag*


__> Post Taxonomies > the taxonomy: post_tag > terms all__

$new->Taxonomies->post_tag->terms->getAll()

*Will return an array of Term objects*


__> Post Taxonomies > the taxonomy: post_tag > the term: tag-1__

$new->Taxonomies->Post_Tag->terms->term('tag-1')

*Will return a single instance of the Taxonomy object for the term tag-1.*


__> Post Taxonomies > the taxonomy: post_tag > the term: tag-1's > it's url__

$new->Taxonomies->Post_Tag->terms->term('tag-1')->url

*Will return the url property - via magic __get() - of the term object for tag-1.*



That's the gist of it. You can experiment with Post->Author, Post->Image, etc.  


> --
>
> If you have any questions, comments, or suggestions, please open an Issue. 
>
> --





### FAQ

__1 - Why?__

Too much time wasted thinking of (WP) data / models as a series of functions got the best of me :) 

__2 - Is this a REST API?__

No. It is not. Nor it is intended to be. That said, when it made sense we try to mirror the REST API's naming convention. For example, ->post_title is now simply ->title (since we already know we're working with the Post object).

__3 - What problem does this solve?__

First, it accuratelyrrors the ER / data structure of the WP database. That is, for example, a post has many taxonomies, a (single) taxonomy can have many terms, and each term has properties. Of course, as you will see, there are other properties and methods on your way from post to term. 
 
There is a now a natur mial and obvious structure / hierarchy, as there should be.

Second, it replaces WP's native sprawl of functions with the modern convenience of objects, methods and properties. While WPezAPI won't attempt to encapsulate __everything__, its classes / structure makes better sense, is cleaner to work with, as well as serve as its own ez goto codex (of sorts).

Third, it attempts to enforce a sense of consistency. For example, when you ask for a url, you get a url; as opposed to asking for a link and sometimes getting back formed markup, and other times getting a single url.

BTW, with rare exception will WPezAPI return markup. It doesn't do views. It's a (super) model.

Fourth, it makes WP ez to learn, and dare I say, more fun again. No more this function and/or that function. Overload be gone! Now it's: "here's an object and here's what you can do with it." 

Again, as it should be. The ezWay.

  



### HELPFUL LINKS

- TODO
 
 
### TODO

- Overview, Setup, more / better Examples

- Use WP_Error?

- Post \ Adjacent

- Comments and (perhaps) Post \ Comments

- add Profile request to Gravatar

- review refact of ClassUser and related user classes 

- Settings

- Other CRUD (as opposed to only GET)


### CHANGE LOG

__-- 0.0.4.3 (16 Nov 2018)__

- Fleshed out ClassEmbedded

__-- 0.0.4.2 (13 Nov 2018)__

- Additional Gravatar* cleanup

__-- 0.0.4.1 (12 Nov 2018)__

- Add: GravatarAvatar and  GravatarProfile
- Gravatar wraps these
- ClassUser now feeling very empowered

__-- 0.0.4.0 (3 Nov 2018)__

- Refactored ClassUser
- ClassGravatar uses ClassImg for the image / img
- Added ClassUserData and ClassUserMeta
- Continued: Working through the media / attachment / image refactoring continues

__-- 0.0.3.1 (3 Nov 2018)__

- Working through the media / attachment / image refactoring continues

__-- 0.0.3.0 (30 Oct 2018)__

- Still in progress > Refactoring of Attachments, Media, Images, Imgs, etc. 
- Flattened the folder structure  

__-- 0.0.2.2 (11 Oct 2018)__

- More User / Author refactoring 


__-- 0.0.2.1 (10 Oct 2018)__

- Introducing the Avatar class (which is used by User / Author, and will eventually be shared with the CommentClass). 


__-- 0.0.2 (9 Oct 2018)__

- Refactoring / enhancing - mostly the Post > Taxonomies > Taxonomy > Terms > Term tree. 

__-- 0.0.1 (28 May 2018)__

- Various refactoring

__-- 0.0.1__

- INIT - And away we go...



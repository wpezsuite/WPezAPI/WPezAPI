<?php

namespace WPezSuite\WPezAPI\Core\Response;


class ClassResponse{

    protected $_str_status;
    protected $_str_code;
    protected $_str_message;
    protected $_arr_data;


    public function __construct() {

        $this->setProperyDefaults();
    }


    protected function setProperyDefaults(){

        $this->_str_status = false;
        $this->_str_code = false;
        $this->_str_message = false;
        $this->_arr_data = [];

    }

    public function setStatus( $str_status = false){

        $this->_str_status = (string) $str_status;

    }

    public function setCode( $str_code = false){

        $this->_str_code = (string) $str_code;

    }

    public function setMessage( $str_msg = false ){

        $this->_str_message = (string) $str_msg;
    }

    public function setData( $arr_data = []){

        $this->_arr_data = (array) $arr_data;

    }

    public function __get( $str_prop ){

        $str_prop = strtolower($str_prop);

        switch ($str_prop){

            case 'status':
                return $this->getStatus();

            case 'code':
                return $this->getCode();

            case 'message':
                return $this->getMessage();

            case 'data':
                return $this->getData();

            default:
                return null;

        }

    }

    public function getStatus(){

        return $this->_str_status;

    }

    public function getCode(){

        return $this->_str_code;

    }

    public function getMessage(){

        return $this->_str_message;

    }

    public function getData(){

        return $this->_str_Data;

    }










}
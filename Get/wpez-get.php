<?php
/*
Plugin Name: STHM Hero (PoC)
Plugin URI:
Description: Proof of concept. Uses ACF
Version: 0.0.1
Author: mfs for Temple STHM
Author URI:
*/

namespace WPezSuite\WPezAPI\Get;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('This plugin - namespace: ' . __NAMESPACE__ . ' - requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}

function autoloader( $bool = true ){

    if ( $bool !== true ) {
        return;
    }

    require_once dirname( dirname(__FILE__)) . '/Core/Autoload/ClassWPezAutoload.php';

    $new_autoload = new \WPezSuite\WPezAPI\Core\ClassWPezAutoload();
    $new_autoload->setPathParent( dirname( __FILE__ ) );
    $new_autoload->setNeedle(__NAMESPACE__ );
    $new_autoload->setReplaceSearch(__NAMESPACE__ . DIRECTORY_SEPARATOR);

    spl_autoload_register( [$new_autoload, 'WPezAutoload'], true );
}
autoloader();
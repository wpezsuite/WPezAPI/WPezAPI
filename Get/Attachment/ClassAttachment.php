<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Attachment;

use WPezSuite\WPezAPI\Get\PostBase\AbstractClassPostBase;

class ClassAttachment extends AbstractClassPostBase {

    use \WPezSuite\WPezAPI\Get\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_int_id;
    protected $_obj_img;

    protected $_str_extension;
    protected $_str_subdir;
    protected $_str_basename;
    protected $_str_filename;
    protected $_str_dirname;

    protected $_arr_types_all;
    protected $_arr_types;
    protected $_str_type;

    protected $_str_desc_filtered;
    protected $_str_attached_file;
    protected $_str_path;

    // ref: https://developer.wordpress.org/reference/functions/wp_get_attachment_image/
    // see More Info section
    protected $_bool_icon;
    protected $_arr_sizes;
    protected $_str_size;
    protected $_arr_get_size;
    protected $_arr_upload_dir;

    protected $_str_uploads_path;
    protected $_str_uploads_url;


    protected $_obj_img_meta;

    public function __construct( $TODO = true ) {

        $this->setPropertyDefaultsAttachment();
        parent::__construct();

    }


    protected function setPropertyDefaultsAttachment() {

        $this->_int_id               = false;
        $this->_obj_img              = false;
        $this->_str_extension        = false;
        $this->_str_subdir           = false;
        $this->_str_basename         = false;
        $this->_str_filename         = false;
        $this->_str_dirname         = false;
        $this->_arr_types_all        = [
            'audio',
            'image',
            'video'
        ];
        $this->_arr_types            = [
            'audio' => null,
            'image' => null,
            'video' => null
        ];
        $this->_str_type             = false;
        $this->_str_desc_filtered    = false;
        $this->_str_attached_file    = false;
        $this->_str_path             = false;
        $this->_arr_upload_dir       = wp_get_upload_dir();
        $this->_str_uploads_path     = false;
        $this->_str_uploads_url      = false;
        $this->_obj_img_meta         = false;
    }


    protected function setPostCheck( $obj_post ) {

        if ( $obj_post->post_type == 'attachment' ) {
            return true;
        }

        return false;
    }

    public function setAttachmentByAttachmentID( $int_id = false ) {

        if ( wp_attachment_is_image( $int_id ) ) {
            return $this->setPostByID( $int_id );
        }

        return false;
    }

 //   protected function magicGetProxyAttachment( $str_prop = '' ) {
    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'caption':
            case 'excerpt':
            case 'post_excerpt':
                return $this->getCaption();

            case 'desc':
            case 'description':
            case 'content':
            case 'post_content':
                return $this->getDesc();

            case 'desc_filtered':
            case 'description_filtered':
            case 'content_filtered':
            case 'post_content_filtered':
                return $this->getDescFiltered();

            // -----------------------
            case 'attached_file':
                return $this->getAttachedFile();

            case 'subdir':
                return $this->getSubdir();

            case 'dirname':
                return $this->getDirname();

            case 'basename':
                return $this->getBasename();

            case 'extension':
                return $this->getExtension();

            case 'filename':
                return $this->getFilename();

            case 'path':
                return $this->getPath();

            case 'uploads_path':
            case 'basepath':
                return $this->getUploadsPath();

            case 'uploads_url':
            case 'baseurl':
                return $this->getUploadsUrl();

            case 'mime_type':
                return $this->getMimeType();

            case 'is_type':
            case 'attachment_is':
                return $this->isType();

            case 'is_audio':
                return $this->isAudio();

            case 'is_image':
                return $this->isImage();

            case 'is_video':
                return $this->isVideo();

            default:
                return parent::__get($str_prop);
        }

    }


    public function getExtension( $mix_fallback = false ) {

        if ( $this->_str_extension === false ) {

            $mix                  = pathinfo( $this->getAttachedFile() );
            $this->_str_extension = $mix_fallback;

            if ( isset( $mix['extension'] ) ) {
                $this->_str_extension = $mix['extension'];
            }

        }

        return $this->_str_extension;

    }

    public function getSubdir( $mix_fallback = false ) {

        if ( $this->_str_subdir === false ) {

            $mix               = pathinfo( $this->getAttachedFile() );
            $this->_str_subdir = $mix_fallback;

            if ( isset( $mix['dirname'] ) ) {
                $this->_str_subdir = $mix['dirname'];
            }

        }

        return $this->_str_subdir;
    }


    public function getBasename( $mix_fallback = false ) {

        if ( $this->_str_basename === false ) {

            $mix                 = pathinfo( $this->getAttachedFile() );
            $this->_str_basename = $mix_fallback;

            if ( isset( $mix['basename'] ) ) {
                $this->_str_basename = $mix['basename'];
            }

        }

        return $this->_str_basename;

    }


    public function getFilename( $mix_fallback = false ) {

        if ( $this->_str_filename === false ) {

            $mix                 = pathinfo( $this->getAttachedFile() );
            $this->_str_filename = $mix_fallback;

            if ( isset( $mix['filename'] ) ) {
                $this->_str_filename = $mix['filename'];
            }

        }

        return $this->_str_filename;
    }


    public function getDirname( $mix_fallback = false ) {

        if ( $this->_str_dirname === false ) {

            $mix                = pathinfo( $this->getPath() );
            $this->_str_dirname = $mix_fallback;

            if ( isset( $mix['dirname'] ) ) {
                $this->_str_dirname = $mix['dirname'];
            }

        }

        return $this->_str_dirname;
    }

    public function getUploadsPath( $todo = false ) {

        if ( $this->_str_uploads_path === false ) {

            $this->_str_uploads_path = $this->getStrUploadDir( 'path' );
        }

        return $this->_str_uploads_path;

    }

    public function getUploadsUrl( $todo = false ) {

        if ( $this->_str_uploads_url === false ) {

            $this->_str_uploads_url = $this->getStrUploadDir( 'url' );
        }

        return $this->_str_uploads_url;

    }

    protected function getStrUploadDir( $str_key ) {

        if ( isset( $this->_arr_upload_dir[ $str_key ] ) && isset( $this->_arr_upload_dir['subdir'] ) ) {

            $str_subdir = strrev( $this->_arr_upload_dir['subdir'] );
            $str_hs     = strrev( $this->_arr_upload_dir[ $str_key ] );

            if ( strpos( $str_hs, $str_subdir ) == 0 ) {

                $str_tmp = substr( $str_hs, strlen( $str_subdir ) );

                return strrev( $str_tmp );
            }

        }

        return false;

    }

    public function isType() {

        if ( $this->_str_type !== false ) {
            return $this->_str_type;
        }

        $this->_str_type = false;
        foreach ( $this->_arr_types_all as $str_type ) {

            $this->_arr_types[ $str_type ] = false;
            // ttps://developer.wordpress.org/reference/functions/wp_attachment_is/
            if ( wp_attachment_is( $str_type, $this->_int_id ) ) {
                $this->_arr_types[ $str_type ] = true;
                $this->_str_type               = $str_type;
            }
        }

        return $this->_str_type;
    }

    public function isAudio() {

        $this->isType();

        return $this->_arr_types['audio'];

    }

    public function isImage() {

        $this->isType();

        return $this->_arr_types['image'];
    }

    public function isVideo() {

        $this->isType();

        return $this->_arr_types['video'];
    }

    public function getAttachedFile( $mix_fallback = false ) {

        if ( $this->_str_attached_file !== false ) {
            return $this->_str_attached_file;
        }

        // https://codex.wordpress.org/Function_Reference/get_attached_file
        $mix_temp = $this->getMeta()->_wp_attached_file;
        if ( $mix_temp === false ) {
            return $mix_fallback;
        }

        $this->_str_attached_file = $mix_temp;

        return $this->_str_attached_file;

    }


    public function getPath( $mix_fallback = false ) {

        if ( $this->_str_path === false ) {

            $mix_up = $this->getUploadsPath();
            $mix_sd = $this->getSubdir();
            $mix_bn = $this->getBasename();

            $this->_str_path = $mix_fallback;
            if ( is_string( $mix_up ) && is_string( $mix_sd ) && is_string( $mix_bn ) ) {

                $this->_str_path = $mix_up . DIRECTORY_SEPARATOR . $mix_sd . DIRECTORY_SEPARATOR . $mix_bn;
            }
        }

        return $this->_str_path;
    }




    public function getDesc( $mix_fallback = '' ) {

        return $this->getPostContent( $mix_fallback );
    }

    // TODO compare w/ post class??
    public function getDescFiltered( $mix_fallback = '' ) {

        return $this->getPostContentFiltered( $mix_fallback );
    }

    public function getCaption( $mix_fallback = '' ) {

        return $this->getPostExcerpt( $mix_fallback );
    }


}

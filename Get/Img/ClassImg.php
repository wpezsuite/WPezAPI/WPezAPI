<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Img;

class ClassImg {

    protected $_arr_img_info_defaults;
    protected $_arr_img_info;
    protected $_arr_img_info_other;
    protected $_int_width;
    protected $_int_height;
    protected $_str_src;
    protected $_str_class;
    protected $_arr_class_explode;
    protected $_str_alt;
    protected $_str_srcset;
    protected $_str_srcset_sizes;

    public function __construct() {

        $this->setPropertyDefaults();
    }


    protected function setPropertyDefaults() {

        $this->_arr_img_info_defaults = [
            'width'  => false,
            'height' => false,
            'src'    => false,
            'class'  => false,
            'alt'    => false,
            'srcset' => false,
            'sizes'  => false,
        ];
        $this->_arr_img_info          = $this->_arr_img_info_defaults;
        $this->_arr_img_info_other    = [];
        $this->_int_width             = $this->_arr_img_info_defaults['width'];
        $this->_int_height            = $this->_arr_img_info_defaults['height'];
        $this->_str_src               = $this->_arr_img_info_defaults['src'];
        $this->_str_class             = $this->_arr_img_info_defaults['class'];
        $this->_arr_class_explode     = [];
        $this->_str_alt               = $this->_arr_img_info_defaults['alt'];
        $this->_str_srcset            = $this->_arr_img_info_defaults['srcset'];
        $this->_str_srcset_sizes      = $this->_arr_img_info_defaults['sizes'];

    }



    /**
     * Pass in an <img...> markup string (e.g., from wp_get_attachment_image()
     * to have to parsed and set
     *
     * @param bool $str
     *
     * @return bool
     */
    public function setImgMarkup( $str = false ) {

        if ( is_string( $str ) ) {

            $str = str_replace( '<img ', '', $str );
            $str = str_replace( '/>', '', $str );
            // parse what's left of the sting into an array of pairs
            $mix = shortcode_parse_atts( $str );

            return $this->setAttrs( $mix );

        }

        return false;
    }

    /**
     * maybe you have your own setImgMarkup() that does the parsing
     *
     * @param array $arr
     *
     * @return bool
     */
    public function loaderAll( $arr = [] ) {

        if ( is_array( $arr ) ) {

            return $this->setAttrs( $arr );

        }

        return false;

    }

    protected function setAttrs( $arr = [] ) {

        if ( is_array( $arr ) ) {
            $arr = array_merge( $this->_arr_img_info_defaults, $arr );
        } else {
            $arr = $this->_arr_img_info_defaults;
        }

        $this->_arr_img_info = $arr;

        $this->setWidth( $arr['width'] );
        $this->setHeight( $arr['height'] );
        $this->setSrc( $arr['src'] );
        $this->setClass( $arr['class'] );
        $this->_arr_class_explode = explode( ' ', $arr['class'] );
        $this->setAlt( $arr['alt'] );
        $this->setSrcset( $arr['srcset'] );
        $this->setSizes( $arr['sizes'] );
        unset( $arr['width'] );
        unset( $arr['height'] );
        unset( $arr['src'] );
        unset( $arr['class'] );
        unset( $arr['alt'] );
        unset( $arr['srcset'] );
        unset( $arr['sizes'] );

        $this->_arr_img_info_other = $arr;

        return true;

    }


    public function setWidth( $int_w = false ) {

        $this->_int_width = absint( $int_w );
    }

    public function setHeight( $int_h = false ) {

        $this->_int_height = absint( $int_h );
    }

    public function setSrc( $str_src = false ) {

        $this->_str_src = (string)$str_src;

    }

    public function setClass( $str_class = false ) {

        $this->_str_class = (string)$str_class;
        $this->_arr_class_explode = explode(' ', $this->_str_class);

    }

    public function setAlt( $str_alt = false ) {

        $this->_str_alt = (string)$str_alt;

    }

    public function setSrcset( $str_srcset = false ) {

        $this->_str_srcset = (string)$str_srcset;
    }

    public function setSizes( $str_srcset_sizes = false ) {

        $this->_str_srcset_sizes = (string)$str_srcset_sizes;

    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'width':
                return $this->getWidth();

            case 'height':
                return $this->getHeight();

            case 'src':
                return $this->getSrc();

            case 'class':
                return $this->getClass();

            case 'alt':
                return $this->getAlt();

            case 'srcset':
                return $this->getSrcset();

            case 'sizes':
                return $this->getSizes();

            default:
                return false;
        }
    }

    public function getImgInfo( $str_other = false ) {

        if ( $str_other === 'other' ) {
            return $this->_arr_img_info_other;
        }

        return $this->_arr_img_info;

    }

    public function getWidth( $mix_fallback = false ) {

        if ( $this->_int_width !== false ) {
            return $this->_int_width;
        }

        return $mix_fallback;

    }

    public function getHeight( $mix_fallback = false ) {

        if ( $this->_int_height !== false ) {
            return $this->_int_height;
        }

        return $mix_fallback;

    }

    public function getSrc( $mix_fallback = false ) {

        if ( $this->_str_src !== false ) {
            return $this->_str_src;
        }

        return $mix_fallback;

    }

    public function getClass( $mix_fallback = false ) {

        if ( $this->_str_class !== false ) {
            return $this->_str_class;
        }

        return $mix_fallback;

    }

    public function getClassExplode( $mix_fallback = [] ) {

        if ( ! empty($this->_arr_class_explode) ) {
            return $this->_arr_class_explode;
        }

        return $mix_fallback;

    }

    public function getAlt( $mix_fallback = false ) {

        if ( $this->_str_alt !== false ) {
            return $this->_str_alt;
        }

        return $mix_fallback;

    }

    public function getSrcset( $mix_fallback = false ) {

        if ( $this->_str_srcset !== false ) {
            return $this->_str_srcset;
        }

        return $mix_fallback;

    }

    public function getSizes( $mix_fallback = false ) {

        if ( $this->_str_srcset_sizes !== false ) {
            return $this->_str_srcset_sizes;
        }

        return $mix_fallback;

    }

}
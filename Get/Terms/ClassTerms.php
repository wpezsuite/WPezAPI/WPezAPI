<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Terms;

use WPezSuite\WPezAPI\Get\Term\ClassTerm;

class ClassTerms {

    protected $_int_post_id;
    protected $_str_taxonomy;
    protected $_arr_get_terms;
    protected $_obj_get_terms_latest;
    protected $_arr_get_terms_args_defaults;

    protected $_arr_all_by_slug;
    protected $_arr_all_by_name;
    protected $_arr_all_by_id;
    protected $_arr_all_by_parent;


    public function __construct( $obj_post = false ) {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_int_post_id                 = false;
        $this->_str_taxonomy                = false;
        $this->_arr_get_terms               = false;
        $this->_obj_get_terms_latest        = false;
        $this->_arr_get_terms_args_defaults = [];

        $this->_arr_all_by_slug   = false;
        $this->_arr_all_by_name   = false;
        $this->_arr_all_by_id     = false;
        $this->_arr_all_by_parent = false;
    }


    public function setPostID( $mix = false ) {

        if ( is_array( $mix ) || is_integer( $mix ) ) {
            $this->_int_post_id = $mix;

            $this->_arr_get_terms_args_defaults['object_ids'] = $mix;

            return true;
        }

        return false;
    }

    public function setTaxonomy( $mix = false ) {

        // TODO - is this a valid tax?
        if ( is_array( $mix ) || is_string( $mix ) ) {
            $this->_str_taxonomy                            = $mix;
            $this->_arr_get_terms_args_defaults['taxonomy'] = $mix;

            return true;
        }

        return false;
    }


    /**
     * IMPORTANT!! only use this if your terms slugs do not use '-', only '_'
     * will work
     *
     * @param $str_slug
     *
     * @return bool|mixed
     */
    public function __get( $str_slug ) {

        return $this->getTerm( $str_slug, 'slug' );

    }

    // TODO - not sure which of these I prefer
    public function getTermBySlug( $str_slug = false ) {

        return $this->getTerm( $str_slug, 'slug' );
    }

    public function term( $str_slug = false ) {

        return $this->getTerm( $str_slug, 'slug' );
    }

    public function get( $str_slug = false ) {

        return $this->getTerm( $str_slug, 'slug' );
    }

    public function getTermByID( $str_id = false ) {

        return $this->getTerm( $str_id, 'id' );

    }

    public function getTermByName( $str_name = false ) {

        return $this->getTerm( $str_name, 'name' );
    }

    /**
     * @param bool $str_mix
     * @param bool $str_by - optional
     *
     * @return bool
     */
    public function getTerm( $str_mix = false, $str_by = false ) {

        // TODO - this too? $str_mix = strtolower( (string)$str_mix );
        $str_by = strtolower( (string)$str_by );
        $this->getAll();

        switch ( $str_by ) {
            case 'id':
                if ( isset( $this->_arr_all_by_id[ $str_mix ] ) && isset( $this->_arr_all_by_slug[ $this->_arr_all_by_id[ $str_mix ] ] ) ) {
                    return $this->_arr_all_by_slug[ $this->_arr_all_by_id[ $str_mix ] ];
                }

                break;

            case 'name':
                if ( isset( $this->_arr_all_by_name[ $str_mix ] ) && isset( $this->_arr_all_by_slug[ $this->_arr_all_by_name [ $str_mix ] ] ) ) {
                    return $this->_arr_all_by_slug[ $this->_arr_all_by_name[ $str_mix ] ];
                }

                break;

            case 'slug':
            default:
                if ( isset( $this->_arr_all_by_slug[ $str_mix ] ) ) {
                    return $this->_arr_all_by_slug[ $str_mix ];
                }

                break;

        }

        return false;

    }


    /**
     * https://codex.wordpress.org/Function_Reference/has_term
     *
     * @param bool $mix_term
     *
     * @return mixed
     */
    public function hasTerm( $mix_term = false ) {

        return has_term( $mix_term, $this->_str_taxonomy, $this->_int_post_id );

    }


    /**
     * Set args for get_terms() along w/ a key so you can getTerms('by_key').
     * This allows you to have boilerplate library of $arr_args to lean on at
     * will for getTerms().
     *
     * @param bool $arr_args
     * @param bool $str_key
     *
     * @return bool
     */
    public function setGetTermsArgs( $arr_args = false, $str_key = false ) {

        if ( is_array( $arr_args ) ) {

            $std_obj = new \stdClass();

            //
            $std_obj->args  = array_merge( $arr_args, $this->_arr_get_terms_args_defaults );
            $std_obj->slugs = false;

            if ( is_string( $str_key ) ) {

                $this->_arr_get_terms[ $str_key ] = $std_obj;
                var_dump( $this->_arr_get_terms );

                return true;
            }

            $this->_obj_get_terms_latest = $std_obj;

            return true;

        }

        return false;
    }

    protected function getTermsSlugs( $args = false ) {

        if ( is_array( $args ) ) {
            $arr_terms = get_terms( $args );

            if ( ! is_array( $arr_terms ) ) {

                // TODO return the WP_Error?
                return false;
            } else {

                $arr_terms_new = $this->rekeyBy( $arr_terms );
                $arr_keys      = array_keys( $arr_terms_new );

                return $arr_keys;
            }
        }

        return false;

    }


    /**
     * Use setGetTermsArgs() first. Note: We don't re-store the whole term
     * obj(s) each time. That's done once in _arr_all_by_slug. What we keep for
     * each getTerms query results is an array of the slugs. That array is
     * matrix'ed against the _arr_all_by_slug to return the whole term obj
     *
     * @param bool $str_key
     *
     * @return array|bool|void
     */
    // TODO - allow $arr_args to be passed in,
    public function getTerms( $str_key = false ) {

        if ( is_string( $str_key ) ) {

            if ( isset( $this->_arr_get_terms[ $str_key ] ) && is_object( $this->_arr_get_terms[ $str_key ] ) ) {

                if ( $this->_arr_get_terms[ $str_key ]->slugs === false ) {

                    $this->_arr_get_terms[ $str_key ]->slugs = $this->getTermsSlugs( $this->_arr_get_terms[ $str_key ]->args );
                }

                if ( is_array( $this->_arr_get_terms[ $str_key ]->slugs ) ) {
                    return $this->mapSlugsToAll( $this->_arr_get_terms[ $str_key ]->slugs );
                } else {
                    return false;
                }

            } else {

                // TODO
                // something is wrong with this key. it's not in the _arr_get_terms[].
                return false;

            }
        }

        if ( is_object( $this->_obj_get_terms_latest ) ) {

            $arr_slugs = $this->getTermsSlugs( $this->_obj_get_terms_latest->args );

            return $this->mapSlugsToAll( $arr_slugs );

        }

        // oh well...return the all
        return $this->getAll();
    }

    /**
     * by Terms - not the plural
     *
     * @param bool $mix
     *
     * @return bool
     */
    public function getTermsByParent( $mix_parent_id = false ) {

        if ( isset( $this->_arr_all_by_parent[ $mix_parent_id ] ) && is_array( $this->_arr_all_by_parent[ $mix_parent_id ] ) ) {

            $arr_ret = [];
            foreach ( $this->_arr_all_by_parent[$mix_parent_id] as $parent => $slug ){

                if ( isset( $this->_arr_all_by_slug[ $slug ] ) && $this->_arr_all_by_slug[ $slug ] instanceof ClassTerm ) {

                    $arr_ret[ $slug ] = $this->_arr_all_by_slug[ $slug ];

               }
            }

            return $arr_ret;
        }

        return false;
    }

    /**
     * @param bool $arr_slugs
     *
     * @return bool|void
     */
    protected function mapSlugsToAll( $arr_slugs = false ) {

        if ( is_array( $arr_slugs ) ) {

            $arr_all = $this->getAll();
            if ( is_array( $arr_all ) ) {

                $arr_new = [];

                foreach ( $arr_slugs as $str_slug ) {
                    if ( isset( $arr_all[ $str_slug ] ) ) {
                        $arr_new[ $str_slug ] = $arr_all[ $str_slug ];
                    }
                }

                return $arr_new;

            }
        }

        return false;
    }

    protected function rekeyBy( $arr_orig = false ) {

        if ( is_array( $arr_orig ) ) {

            $arr_new = [];
            foreach ( $arr_orig as $term => $obj_term ) {

                if ( $obj_term instanceof \WP_Term ) {
                    $new = new ClassTerm();
                    $new->setTermObject( $obj_term );
                    $arr_new[ $obj_term->slug ] = $new;
                    // for _by_id, _by_name and _by_parent, we only need to store a key
                    // to slug "map". when we need one of these we use the map to get the
                    // slug and then from by_slug to get the obj
                    $this->_arr_all_by_id[ $obj_term->term_id ]      = $obj_term->slug;
                    $this->_arr_all_by_name[ $obj_term->name ]       = $obj_term->slug;
                    $this->_arr_all_by_parent[ $obj_term->parent ][] = $obj_term->slug;
                }
            }

            return $arr_new;
        }

        return false;
    }

    public function getAllNames() {

        $this->getAll();

        if ( is_array( $this->_arr_all_by_name ) ) {

            return $this->_arr_all_by_name;
        }

        return [];
    }

    public function getAllIDs() {

        $this->getAll();

        if ( is_array( $this->_arr_all_by_id ) ) {

            return $this->_arr_all_by_id;
        }

        return [];
    }

    /**
     * Gets all the terms for the taxonomy
     *
     * @return array|bool
     */
    public function getAll() {

        if ( is_array( $this->_arr_all_by_slug ) ) {
            return $this->_arr_all_by_slug;
        }

        // https://developer.wordpress.org/reference/functions/get_terms/
        $arr_all = get_terms( $this->_arr_get_terms_args_defaults );

        if ( is_array( $arr_all ) ) {

            $this->_arr_all_by_slug = $this->rekeyBy( $arr_all );

        } else {

            // TODO WP_Error
            $this->_arr_all_by_slug = [];
        }

        return $this->_arr_all_by_slug;
    }

    /**
     * We don't like outputting pre-fab markup - and certainly not from a model
     * - but... think of this as a quick & dirty prototyping tool. But! Never
     * more than that!!
     *
     * @param string $str_before
     * @param string $str_after
     * @param string $str_sep
     *
     * @return mixed
     */
    public function getTheTermsList( $str_before = '', $str_after = '', $str_sep = ', ' ) {

        // https://codex.wordpress.org/Function_Reference/get_the_term_list
        // https://codex.wordpress.org/Function_Reference/the_terms
        return get_the_term_list( $this->_int_post_id, $this->_str_taxonomy, $str_before, $str_sep, $str_after );

    }

}
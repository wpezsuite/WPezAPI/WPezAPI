<?php
/**
 * WordPress' get_avatar() function wrapped up as an ez to use class. Establish
 * so it can be reference from a post (object) or a comment (object), or stand
 * alone. https://codex.wordpress.org/Function_Reference/get_avatar
 */

/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\GravatarAvatar;

use WPezSuite\WPezAPI\Get\Img\ClassImg;


class ClassGravatarAvatar {

    protected $_mix_by_value;
    protected $_str_by_type;
    protected $_str_user_email;
    protected $_str_profile_qr_width;

    protected $_arr_arg_ratings;
    protected $_arr_arg_default;

    protected $_int_parm_size;
    protected $_str_parm_default_url;
    protected $_str_parm_alt;

    protected $_arr_args;
    protected $_int_arg_size;
    protected $_int_arg_height;
    protected $_int_arg_width;
    protected $_str_arg_default;
    protected $_bool_arg_force_default;
    protected $_str_arg_rating;
    protected $_str_arg_scheme;
    protected $_mix_arg_class;
    protected $_bool_arg_force_display;
    protected $_str_arg_extra_attr;

    protected $_str_img;
    protected $_arr_img_info;
    protected $_obj_img;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_str_user_email         = false;
        $this->_str_profile_qr_width = '';
        $this->_arr_arg_ratings        = [ 'G', 'PG', 'R', 'X' ];
        $this->_arr_arg_default        = [ '404', 'retro', 'monsterid', 'wavatar', 'indenticon', 'mystery', 'mm', 'mysteryman', 'blank', 'gravatar_default' ];
        $this->_int_parm_size          = 96;
        $this->_str_parm_default_url   = null;
        $this->_str_parm_alt           = '';
        $this->_arr_args               = [];
        $this->_int_arg_size           = 96;
        $this->_int_arg_height         = null;
        $this->_int_arg_width          = null;
        $this->_str_arg_default        = null;  // default: get_option( 'avatar_default', 'mystery' ),
        $this->_bool_arg_force_default = false;
        $this->_str_arg_rating         = null;  // default: get_option( 'avatar_rating' ),
        $this->_str_arg_scheme         = null;
        $this->_mix_arg_class          = null;
        $this->_bool_arg_force_display = false;
        $this->_str_arg_extra_attr     = '';
        $this->_str_img                = false;
        $this->_arr_img_info           = false;
        $this->_obj_img                = false;


    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $mix_id
     *
     * @return bool
     */
    public function setUserID( $mix_id = false ) {

        if ( get_avatar_url( $mix_id ) !== false ) {

            $this->_mix_by_value = trim((string) $mix_id);
            $this->_str_by_type  = 'user_id';

            return true;

        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_email
     *
     * @return bool
     */
    public function setUserEmail( $str_email = false ) {

        $str_email = trim( $str_email );
        if ( get_avatar_url( $str_email ) !== false ) {

            $this->_mix_by_value   = $str_email;
            $this->_str_user_email = $str_email;
            $this->_str_by_type    = 'user_email';

            return true;

        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $obj_comment
     *
     * @return bool
     */
    public function setCommentObject( $obj_comment = false ) {

        if ( $obj_comment instanceof \WP_Comment ) {
            // TODO - test to see if avatar exists

            $this->_mix_by_value = $obj_comment;
            $this->_str_by_type  = 'wp_comment';

            return true;

        }

        return false;

    }


    /**
     * As used in the native get_avatar() function
     *
     * @param string $int_value
     *
     * @return bool
     */
    public function setParmSize( $int_value = '' ) {

        if ( ! empty( $int_value ) ) {
            $this->_int_parm_size = (integer)$int_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_value
     *
     * @return bool
     */
    public function setParmDefault( $str_value = false ) {

        if ( is_string( $str_value ) ) {
            // TODO - validate for URL?
            $this->_str_parm_default_url = (string)$str_value;

            return true;
        }

        return false;

    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_value
     *
     * @return bool
     */
    public function setParmAlt( $str_value = false ) {

        if ( is_string( $str_value ) ) {
            $this->_str_parm_alt = (string)$str_value;

            return true;
        }

        return false;

    }

    /**
     * A slightly different way to set the get_avatar() parmeters
     *
     * @param $str_name
     * @param $mix_value
     *
     * @return bool
     */
    public function setParameter( $str_name, $mix_value ) {

        $str_name = strtolower( $str_name );

        switch ( $str_name ) {

            case 'size':

                return $this->setParmSize( $mix_value );

            case 'default':

                return $this->setParmDefault( $mix_value );

            case 'alt':

                return $this->setParmAlt( $mix_value );
        }

        return false;
    }


    // Args

    /**
     * As used in the native get_avatar() function
     *
     * @param string $int_value
     *
     * @return bool
     */
    public function setArgSize( $int_value = '' ) {

        if ( ! empty( $int_value ) ) {
            $this->_int_arg_size = (integer)$int_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param string $int_value
     *
     * @return bool
     */
    public function setArgHeight( $int_value = '' ) {

        if ( ! empty( $int_value ) ) {
            $this->_int_arg_height = (integer)$int_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param string $int_value
     *
     * @return bool
     */
    public function setArgWidth( $int_value = '' ) {

        if ( ! empty( $int_value ) ) {
            $this->_int_arg_width = (integer)$int_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_value
     *
     * @return bool
     */
    public function setArgDefault( $str_value = false ) {

        if ( is_string( $str_value ) ) {
            $str_value = strtolower( $str_value );
            if ( in_array( $str_value, $this->_arr_arg_default ) ) {
                $this->_str_arg_default = $str_value;

            } else {
                // TODO validate for url to image? << easier said than done?
                $this->_str_arg_default = $str_value;
            }

            return true;
        }

        return false;

    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $bool_value
     *
     * @return bool
     */
    public function setArgForceDefault( $bool_value = false ) {

        if ( ! empty( $bool_value ) ) {
            $this->_bool_arg_force_default = (boolean)$bool_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param string $str_value
     *
     * @return bool
     */
    public function setArgRating( $str_value = '' ) {

        $str_value = strtoupper( $str_value );
        if ( in_array( $str_value, $this->_arr_arg_ratings ) ) {
            $this->_str_arg_rating = (string)$str_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $str_value
     *
     * @return bool
     */
    public function setArgScheme( $str_value = false ) {

        if ( is_string( $str_value ) ) {
            // TODO - validate for URL?
            $this->_str_arg_scheme = (string)$str_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $mix_value
     *
     * @return bool
     */
    public function setArgClass( $mix_value = false ) {

        if ( is_string( $mix_value ) || is_array( $mix_value ) ) {
            $this->_mix_arg_class = $mix_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param bool $bool_value
     *
     * @return bool
     */
    public function setArgForceDisplay( $bool_value = false ) {

        if ( ! empty( $bool_value ) ) {
            $this->_bool_arg_force_display = (boolean)$bool_value;

            return true;
        }

        return false;
    }

    /**
     * As used in the native get_avatar() function
     *
     * @param string $str_value
     * @param bool   $bool_esc
     *
     * @return bool
     */
    public function setArgExtraAttr( $str_value = '', $bool_esc = true ) {

        if ( is_string( $str_value ) ) {

            $this->_str_arg_extra_attr = esc_attr( $str_value );

            if ( $bool_esc === false ) {

                $this->_str_arg_extra_attr = $str_value;
            }

            return true;
        }

        return false;
    }


    /**
     * A slightly different way to set the get_avatage() args
     *
     * @param $str_name
     * @param $mix_value
     *
     * @return bool
     */
    public function setArg( $str_name, $mix_value ) {

        $str_name = strtolower( $str_name );

        switch ( $str_name ) {

            case 'size':
                return $this->setArgSize( $mix_value );


            case 'height':
                return $this->setArgHeight( $mix_value );


            case 'width':
                return $this->setArgWidth( $mix_value );

            case 'default':

                return $this->setArgDefault( $mix_value );

            case 'force_default':

                return $this->setArgForceDefault( $mix_value );

            case 'rating':

                return $this->setArgRating( $mix_value );


            case 'scheme':
                return $this->setArgScheme( $mix_value );

            case 'class':

                return $this->setArgClass( $mix_value );

            case 'force_display':

                return $this->setArgForceDisplay( $mix_value );

            case 'extra_attr':

                return $this->setArgExtraAttr( $mix_value );

        }

        return false;

    }

    protected function getArrArgs() {

        $args = [
            'size'          => $this->_int_arg_size,
            'height'        => $this->_int_arg_height,
            'width'         => $this->_int_arg_width,
            'default'       => $this->_str_arg_default,
            'force_default' => $this->_bool_arg_force_default,
            'rating'        => $this->_str_arg_rating,
            'scheme'        => $this->_str_arg_scheme,
            'class'         => $this->_mix_arg_class,
            'force_display' => $this->_bool_arg_force_display,
            'extra_attr'    => $this->_str_arg_extra_attr
        ];

        // remove any pairs where the value is null or empty
        $arr = array_filter( $args, function ( $v, $k ) {

            return ! is_null( $v ) && ! empty( $v );
        }, ARRAY_FILTER_USE_BOTH );

        return $arr;

    }

    /**
     * Or set the get_avatage() $args in one fell swoop
     *
     * @param array $arr_args
     *
     * @return bool
     */
    public function setArgs( $arr_args = [] ) {

        if ( is_array( $arr_args ) ) {

            $this->_arr_args = $arr_args;

            return true;
        }

        return false;

    }

    /**
     * magic gets for the object we generate using get_avatar()
     *
     * @param $str_prop
     *
     * @return bool|string
     */
    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        $mix = $this->getAll( 'array' );
        if ( is_array( $mix ) ) {

            if ( $str_prop === 'src' ) {
                $str_prop = 'url';
            }

            if ( $str_prop === 'img' ) {

                return $this->getImg();

            }

            if ( isset( $mix[ $str_prop ] ) ) {

                return trim( $mix[ $str_prop ] );
            } else {
                return false;
            }

        }

        return false;
    }

    protected function getImg() {

        if ( $this->_obj_img === false ) {
            $mix = $this->getAll( 'markup' );
            if ( $mix !== false ) {
                $new = new ClassImg();
                $new->setImgMarkup( $mix );
                $this->_obj_img = $new;
            }

        }

        return $this->_obj_img;

    }

    /**
     * Get the avatar img's alt, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getAlt( $str_fallback = '' ) {

        return $this->getMaster( 'alt', $str_fallback );

    }

    /**
     * Get the avatar img's srcset, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getSrcSet( $str_fallback = '' ) {

        return $this->getMaster( 'srcset', $str_fallback );

    }

    /**
     * Get the avatar img's class, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getClass( $str_fallback = '' ) {

        return $this->getMaster( 'class', $str_fallback );

    }

    /**
     * Get the avatar img's height, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getHeight( $str_fallback = '' ) {

        return $this->getMaster( 'height', $str_fallback );

    }

    /**
     * Get the avatar img's width, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getWidth( $str_fallback = '' ) {

        return $this->getMaster( 'width', $str_fallback );

    }

    /**
     * Get the avatar img's url, or use the fallback
     *
     * @param string $str_fallback
     *
     * @return mixed
     */
    public function getUrl( $str_fallback = '' ) {

        return $this->getMaster( 'url', $str_fallback );
    }


    protected function getMaster( $str_prop, $str_fallback ) {

        if ( isset( $this->getAll()->$str_prop ) && ! empty( $this->getAll()->$str_prop ) ) {
            return $this->getAll()->$str_prop;
        }

        // use the fallback value if ! set() or is empty()
        return $str_fallback;

    }


    /**
     * Returns the avatar as an object (default), array, or markup string (but
     * we naturally frown upon the latter (sans for quick & dirty built out /
     * prototyping)
     *
     * @param bool $str_return
     *
     * @return bool|object
     */
    public function getAll( $str_return = false ) {

        $args = $this->getArrArgs();
        if ( ! empty( $this->_arr_args ) ) {
            $args = $this->_arr_args;
        }

        if ( $this->_arr_img_info === false ) {

            $mix = get_avatar( $this->_mix_by_value, $this->_int_parm_size, $this->_str_parm_default_url, $this->_str_parm_alt, $args );

            if ( $mix === false ) {

                return false;

            } else {

                $this->_str_img = $mix;
                // strip off the img tag junk
                $mix = str_replace( ' < img ', '', $mix );
                $mix = str_replace( ' />', '', $mix );
                // parse what's left of the sting into an array of pairs
                $this->_arr_img_info = shortcode_parse_atts( $mix );

                // TODO - breakdown the class to determine what was returned, add a property for that.
                // TOTO - add a method isSomething() to make it ez to know what you have
                $this->_arr_img_info['avatar-default'] = false;
                if ( isset( $this->_arr_img_info['class'] ) && strpos( $this->_arr_img_info['class'], 'avatar-default' ) !== false ) {
                    //
                    $this->_arr_img_info['avatar-default'] = true;

                }
                if ( isset( $this->_arr_img_info['src'] ) ) {

                    $this->_arr_img_info['url'] = $this->_arr_img_info['src'];
                    unset( $this->_arr_img_info['src'] );
                }
            }
        }

        if ( strtolower( $str_return ) === 'markup' ) {

            return $this->_str_img;
        } elseif
        ( strtolower( $str_return ) === 'array' ) {
            return $this->_arr_img_info;
        }

        return (object)$this->_arr_img_info;

    }
}
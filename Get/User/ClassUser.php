<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\User;

use WPezSuite\WPezAPI\Get\UserData\ClassUserData;
use WPezSuite\WPezAPI\Get\UserMeta\ClassUserMeta;
use WPezSuite\WPezAPI\Get\Gravatar\ClassGravatar;


class ClassUser {

    protected $_mix_ret;

    protected $_int_id;
    protected $_obj_user;

    protected $_obj_user_data;
    protected $_obj_user_meta;
    protected $_obj_gravatar;

    // protected $_str_gravatar_base_url;

    public function __construct() {

        $this->setPropertyDefaults();

    }


    protected function setPropertyDefaults() {

        $this->_mix_ret       = false;
        $this->_int_id        = false;
        $this->_obj_user      = false;
        $this->_obj_user_data = false;
        $this->_obj_user_meta = false;
        $this->_obj_gravatar  = false;

        // $this->_str_gravatar_base_url = 'https://secure.gravatar.com/avatar/';
    }

    public function setUserByID( $mix = false ) {

        if ( is_string( $mix ) ) {

            if ( $mix instanceof \WP_User ) {

                $int_id = absint( $mix->ID );
            } else {

                $int_id = absint( $mix );
            }

            return $this->setUserByField( 'id', $int_id );
        }

        return false;
    }

    public function setUserByNiceName( $mix = false ) {

        return $this->setUserBySlug( $mix );
    }

    public function setUserBySlug( $mix = false ) {

        if ( is_string( $mix ) ) {

            if ( $mix instanceof \WP_User ) {

                $str_slug = $mix->user_nicename;
            } else {

                $str_slug = (string)$mix;
            }

            return $this->setUserByField( 'slug', $str_slug );
        }

        return false;
    }

    public function setUserByEmail( $mix = false ) {

        if ( is_string( $mix ) ) {

            if ( $mix instanceof \WP_User ) {

                $str_email = $mix->user_email;
            } else {

                $str_email = (string)$mix;
            }

            return $this->setUserByField( 'email', $str_email );
        }

        return false;
    }

    public function setUserByLogin( $mix = false ) {

        if ( is_string( $mix ) ) {

            if ( $mix instanceof \WP_User ) {

                $str_login = $mix->user_login;
            } else {

                $str_login = (string)$mix;
            }

            return $this->setUserByField( 'login', $str_login );
        }

        return false;
    }


    protected function setUserByField( $str_field, $mix_value ) {

        $mix_get_user_by = $this->getUserBy( $str_field, $mix_value );

        if ( $mix_get_user_by instanceof \WP_User && isset( $mix_get_user_by->data ) ) {

            $this->_int_id   = $mix_get_user_by->ID;
            $this->_obj_user = $mix_get_user_by;

            return true;
        }

        return false;
    }

    /**
     * Because get_user_by() is plugable:
     * https://wordpress.stackexchange.com/questions/134796/get-userdata-inside-custom-build-plugin
     *
     * ref: https://developer.wordpress.org/reference/functions/get_user_by/
     *
     * @param $field
     * @param $value
     *
     * @return bool|\WP_User
     */
    protected function getUserBy( $field, $value ) {

        $userdata = \WP_User::get_data_by( $field, $value );

        if ( ! $userdata ) {
            return false;
        }

        $user = new \WP_User();
        $user->init( $userdata );

        return $user;
    }

    public function setForBlog( $int_blog_id ) {

        $this->_obj_user->for_site( $int_blog_id );

    }

    public function setForSite( $int_site_id ) {

        $this->_obj_user->for_site( $int_site_id );

    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                return $this->getID();

            case 'exists':
                return $this->getExists();

            case 'caps';
                return $this->getCaps();

            case 'cap_key';
                return $this->getCapKey();

            case 'site_id':
                return $this->gerSiteID();

            case 'roles';
                return $this->getRoles();

            case 'all_caps';
                return $this->getAllCaps();

            case 'url':
                return 'TODO'; // ' - url to user profile. __not__ the same as url_posts';

            case 'slug':
                return 'TODO';


            case 'url_edit':
            case 'edit_url':
            case 'edit_user_link':
                return $this->getEditUserLink();

            // --- we'll offer these user data properties here so you don't have to worry about where to find them.  ----
            // --- to use the get*() methods you'll have to use the User Data object available below

            case 'login':
            case 'password':
            case 'pass':
            case 'name_nice':
            case 'nicename':
            case 'nice_name':
            case 'namenice':
            case 'email':
            case 'website':
            case 'website_url':
            case 'url_website':
            case 'user_url';
            case 'registered':
            case 'registered_date':
            case 'registered_year':
            case 'registed_month':
            case 'registered_day';
            case 'registered_time';
            case 'activation_key':
            case 'status':
            case 'name_display':
            case 'display_name':

                if ( $this->getData() instanceof ClassUserData ) {
                    return $this->getData()->$str_prop;
                }

                return $this->_mix_ret;

            // --- we'll offer these user meta properties here so you don't have to worry about where to find them.  ----
            // --- to use the get*() methods you'll have to use the User Meta object available below

            case 'first_name':
            case 'name_first':
            case 'last_name':
            case 'name_last':
            case 'name':
            case 'first_last_name':
            case 'name_first_last':
            case 'nickname':
            case 'name_nick':
            case 'name_nickname':
            case 'desc':
            case 'bio':
            case 'bio_info':
            case 'bio_desc':
            case 'description':
            case 'capabilities':
            case 'wp_capabilities':
            case 'admin_color':
            case 'closedpostboxes_page':
            case 'primary_blog':
            case 'rich_editing':
            case 'source_domain':
                if ( $this->getMeta() instanceof ClassUserMeta ) {
                    return $this->getMeta()->$str_prop;
                }

                return $this->_mix_ret;

            // -------object ---------

            case 'data':
                // case 'Data':
                return $this->getData();

            // --- avatar obj
            case 'gravatar':
                return $this->getGravatar();

            // --- user meta + meta obj
            case 'meta':
                //   case 'Meta':
                return $this->getMeta();

            // TODO have the defaults try to find a user property if not return _mix_ret
            default:
                return $this->_mix_ret;
        }
    }




    // Determine whether a property or meta key is set:
    // TODO = hasProp()
    public function hasProperty( $str = 'TODO' ) {

        return $str;
    }


    // Determine whether user has a cap
    public function hasCap( $str_cap = '' ) {

        return $this->_obj_user->has_cap( $str_cap );
    }

    public function getRoleCaps() {


        return $this->_obj_user->get_role_caps();
    }


    public function getID( $mix_fallback = null ) {

        if ( isset( $this->_obj_user->ID ) ) {
            return $this->_obj_user->ID;
        }

        if ( $mix_fallback !== null ) {
            return $mix_fallback;
        }

        return $this->_mix_ret;

    }

    public function getExists( $mix = '' ) {

        return $this->_obj_user->exists();

    }

    public function getCaps( $mix_fallback = [] ) {

        if ( isset( $this->_obj_user->caps ) ) {
            return $this->_obj_user->caps;
        }

        return $mix_fallback;
    }

    public function getCapKey( $mix_fallback = '' ) {

        if ( isset( $this->_obj_user->cap_key ) ) {
            return $this->_obj_user->cap_key;
        }

        return $mix_fallback;
    }


    public function gerSiteID() {

        return $this->_obj_user->getSiteID();

    }

    public function getRoles( $mix_fallback = [] ) {

        if ( isset( $this->_obj_user->roles ) ) {
            return $this->_obj_user->roles;
        }

        return $mix_fallback;
    }


    public function getAllCaps( $mix_fallback = [] ) {

        if ( isset( $this->_obj_user->allcaps ) ) {
            return $this->_obj_user->allcaps;
        }

        return $mix_fallback;
    }


    /**
     *
     * @return bool
     */
    public function getEditUserLink() {

        if ( $this->_int_id === false ) {
            return false;
        }

        return get_edit_user_link( $this->_int_id );
    }


    public function getData() {

        if ( ! ( $this->_obj_user_data instanceof ClassUserData ) ) {

            if ( isset( $this->_obj_user->data ) && $this->_obj_user->data instanceof \stdClass ) {
                $new  = new ClassUserData();
                $bool = $new->setData( $this->_obj_user->data );
                if ( $bool === true ) {
                    $this->_obj_user_data = $new;
                }
            }
        }

        return $this->_obj_user_data;
    }

    // TODO allow adding query string args
    public function getGravatar() {

        if ( ! ( $this->_obj_gravatar instanceof ClassGravatar ) ) {

            if ( $this->getData() instanceof ClassUserData ) {
                $str = $this->getData()->email;
                if ( is_string( $str ) && ! empty( $str ) ) {

                    $new = new ClassGravatar();
                    $bool = $new->setUserEmail( $str );
                    if ( $bool === true ) {
                        $this->_obj_gravatar = $new;
                    }
                }
            }
        }

        return $this->_obj_gravatar;

    }


    protected function getMeta() {


        if ( $this->_int_id === false ) {

            return false;
        }

        // do we already have meta?
        if ( ! ( $this->_obj_user_meta instanceof ClassUserMeta ) ) {

            $new  = new ClassUserMeta();
            $bool = $new->setUserByID( $this->_int_id );
            if ( $bool === true ) {
                $this->_obj_user_meta = $new;
            }
        }

        return $this->_obj_user_meta;
    }


}
<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Attached;


class ClassAttached {

    protected $_int_post_id;
    protected $_str_mime_type;
    protected $_str_status;
    protected $_bool_active;
    protected $_arr_all;
    protected $_arr_audio;
    protected $_arr_image;
    protected $_arr_video;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_int_post_id;
        $this->_str_mime_type = '*';
        $this->_str_status    = 'any';
        $this->_bool_active   = false;
        $this->_arr_all       = false;
        $this->_arr_audio     = false;
        $this->_arr_image     = false;
        $this->_arr_video     = false;

    }

    public function setPostByID( $mix = false, $str_post_type = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_Post ) {

                $mix_get_post = $mix;

            } else {
                $mix_get_post = get_post( $mix );
            }

            if ( $mix_get_post instanceof \WP_Post && $this->setPostCheck( $mix_get_post ) ) {
                if ( is_string( $str_post_type ) && $mix_get_post->post_type !== $str_post_type ) {

                    $this->_bool_active = false;
                } else {

                    $this->_int_post_id = $mix_get_post->ID;
                    $this->_bool_active = true;
                }
            }
        }

        return $this->_bool_active;
    }

    /**
     * TODO - make more exact
     *
     * @param $obj_post
     *
     * @return bool
     */
    protected function setPostCheck( $obj_post ) {

        if ( $obj_post->post_type !== 'attachment' ) {
            return true;
        }

        return false;
    }

    public function setMimeType( $str_mime_type = '*' ) {

        if ( is_string( $str_mime_type ) ) {

            $this->_str_mime_type = $str_mime_type;

            return true;
        }

        return false;
    }

    public function setStatus( $str_status = 'any' ) {

        if ( is_string( $str_status ) ) {

            $this->_str_status = $str_status;

            return true;
        }

        return false;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'all':
                return $this->getAll();

            case 'audio':
                return $this->getAudio();

            case 'images':
                return $this->getImages();

            case 'videos':
                return $this->getVideos();

            default:
                return false;

        }
    }

    // https://codex.wordpress.org/Function_Reference/get_children
    // https://developer.wordpress.org/reference/functions/get_children/
    // https://codex.wordpress.org/Function_Reference/get_attached_media
    // https://wpsmackdown.com/wordpress-display-all-images-attached-to-post-page/
    // https://www.wpbeginner.com/plugins/14-best-featured-images-plugin-and-tools-for-wordpress/

    public function getAll( $output = OBJECT ) {

        if ( $this->_arr_all === false ) {

            $args           = [
                'post_parent'    => $this->_int_post_id,
                'post_type'      => 'attachment',
                'post_mime_type' => $this->_str_mime_type,
                'numberposts'    => -1,
                'post_status'    => $this->_str_status
            ];
            $this->_arr_all = get_children( $args, $output );
        }

        return $this->_arr_all;
    }


    public function getType( $str_type = 'audio' ) {

        $str_type = strtolower( $str_type );

        switch ( $str_type ) {

            case 'audio':
                return $this->getAudio();

            case 'image':
            case 'images':
                return $this->getImages();

            case 'video':
            case 'videos':
                return $this->getVideos();

            default:
                // TODO
                return false;

        }
    }


    public function getAudio() {

        if ( $this->_arr_audio === false ) {

            $this->_arr_audio = get_attached_media( 'audio', $this->_int_post_id );

        }

        return $this->_arr_audio;
    }


    public function getImages() {

        if ( $this->_arr_image === false ) {

            $this->_arr_image = get_attached_media( 'image', $this->_int_post_id );

        }

        return $this->_arr_image;
    }


    public function getVideos() {

        if ( $this->_arr_video === false ) {

            $this->_arr_video = get_attached_media( 'video', $this->_int_post_id );

        }

        return $this->_arr_video;
    }

}
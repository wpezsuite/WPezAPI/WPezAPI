<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Embedded;


class ClassEmbedded {

    protected $_bool_active;
    protected $_bool_filter_content;
    protected $_int_post_id;
    protected $_obj_post;
    protected $_arr_media_types;
    protected $_arr_all;
    protected $_arr_audio;
    protected $_arr_embed;
    protected $_arr_iframe;
    protected $_arr_img;
    protected $_arr_object;
    protected $_arr_video;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    public function setPropertyDefaults() {

        $this->_bool_filter_content = true;
        $this->_int_post_id         = false;
        $this->_bool_active         = false;
        $this->_arr_media_types     = [ 'audio', 'embed', 'iframe', 'img', 'object', 'video' ];
        $this->_arr_all             = false;
        $this->_arr_audio           = false;
        $this->_arr_embed           = false;
        $this->_arr_iframe          = false;
        $this->_arr_img             = false;
        $this->_arr_object          = false;
        $this->_arr_video           = false;
    }

    public function setFilterContent( $bool_filter_content = true ) {

        $this->_bool_filter_content = (bool)$bool_filter_content;

        return true;
    }


    public function setPost( $obj_post = false, $str_post_type = false ) {

        if ( $obj_post instanceof \WP_Post ) {

            return $this->setPostMaster( $obj_post, $str_post_type );

        }

        return false;
    }


    public function setPostByID( $int_post_id = false, $str_post_type = false ) {

        $int_post_id = (integer)$int_post_id;
        $obj_post    = get_post( $int_post_id );

        if ( $obj_post instanceof \WP_Post ) {

            return $this->setPostMaster( $obj_post, $str_post_type );

        }

        return $this->_bool_active;
    }


    protected function setPostMaster( $obj_post, $str_post_type ) {

        if ( $obj_post instanceof \WP_Post && $this->setPostCheck( $obj_post ) ) {
            // check the post_type?
            if ( is_string( $str_post_type ) && $obj_post->post_type !== $str_post_type ) {

                $this->_bool_active = false;
            } else {

                $this->_obj_post    = $obj_post;
                $this->_bool_active = true;
            }
        }

        return $this->_bool_active;
    }

    /**
     * TODO - make more exact
     *
     * @param $obj_post
     *
     * @return bool
     */
    protected function setPostCheck( $obj_post ) {

        if ( $obj_post->post_type !== 'attachment' ) {
            return true;
        }

        return false;
    }


    public function setMediaTypes( $arr_media_types = [] ) {

        if ( is_array( $arr_media_types ) ) {
            $this->_arr_media_types = array_intersect( $this->_arr_media_types, $arr_media_types );

            return true;
        }

        return false;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'all':
                return $this->getAll();

            case 'audio':
                if ( in_array( 'audio', $this->_arr_media_types ) ) {
                    return $this->getAudio();
                }

                return [];

            case 'embeds':
                if ( in_array( 'embed', $this->_arr_media_types ) ) {
                    return $this->getEmbeds();
                }

                return [];

            case 'iframes':
                if ( in_array( 'iframe', $this->_arr_media_types ) ) {
                    return $this->getIframes();
                }

                return [];

            case 'images':
                if ( in_array( 'img', $this->_arr_media_types ) ) {
                    return $this->getImages();
                }

                return [];

            case 'objects':
                if ( in_array( 'object', $this->_arr_media_types ) ) {
                    return $this->getObjects();
                }

                return [];

            case 'videos':

                if ( in_array( 'video', $this->_arr_media_types ) ) {
                    return $this->getObjects();
                }

                return [];

            default:
                return [];


        }

    }

    public function getAudio() {

        if ( $this->_arr_audio === false ) {

            $this->_arr_audio = $this->getTypeMaster( 'audio' );
        }

        return $this->_arr_audio;

    }


    public function getEmbeds() {

        if ( $this->_arr_embed === false ) {

            $this->_arr_embed = $this->getTypeMaster( 'embed' );
        }

        return $this->_arr_embed;

    }

    public function getIframes() {

        if ( $this->_arr_iframe === false ) {

            $this->_arr_iframe = $this->getTypeMaster( 'iframe' );
        }

        return $this->_arr_iframe;

    }

    public function getImages() {

        if ( $this->_arr_img === false ) {

            $this->_arr_img = $this->getTypeMaster( 'img' );
        }

        return $this->_arr_img;

    }

    public function getObjects() {

        if ( $this->_arr_object === false ) {

            $this->_arr_object = $this->getTypeMaster( 'object' );
        }

        return $this->_arr_object;

    }

    public function getVideos() {

        if ( $this->_arr_video === false ) {

            $this->_arr_video = $this->getTypeMaster( 'object' );
        }

        return $this->_arr_video;

    }


    protected function getTypeMaster( $str_type = '' ) {

        $arr_ret = [];
        $arr_all = $this->getAll();
        if ( is_array( $arr_all ) ) {
            foreach ( $arr_all as $arr_single ) {

                if ( is_array( $arr_single ) && isset( $arr_single['type'] ) ) {

                    if ( $arr_single['type'] === $str_type ) {

                        $arr_ret[] = $arr_single;
                    }
                }
            }

        }

        return $arr_ret;
    }

    public function getAll() {

        if ( $this->_arr_all === false ) {

            $str_content = $this->_obj_post->post_content;
            // $str_content = do_shortcode( $str_content );

            if ( $this->_bool_filter_content !== false ) {
                $str_content = apply_filters( 'the_content', $str_content );
            }

            $this->_arr_all = $this->getMediaEmbedded( $str_content );

        }

        return $this->_arr_all;

    }


    protected function getMediaEmbedded( $content ) {

        // https://developer.wordpress.org/reference/functions/get_media_embedded_in_content/

        $arr_embedded = [];
        $this->_arr_media_types;

        $str_tags = implode( '|', $this->_arr_media_types );

        // TODO - check for tags that are not all lower case?
        if ( preg_match_all( '#<(?P<tag>' . $str_tags . ')[^<]*?(?:>[\s\S]*?<\/(?P=tag)>|\s*\/>)#', $content, $arr_matches ) ) {
            foreach ( $arr_matches[0] as $ndx => $str_match ) {

                $mix_type = $this->getType( $str_match );
                if ( is_string( $mix_type ) ) {
                    $arr_embedded[] = [
                        'orig_order' => $ndx,
                        'type'       => $mix_type,
                        'embed'      => $str_match
                    ];
                }
            }
        }

        return $arr_embedded;
    }

    protected function getType( $str_embed = '' ) {

        // https://stackoverflow.com/questions/2476789/how-to-get-the-first-word-of-a-sentence-in-php
        // http://php.net/strtok
        $str_embed = strtok( $str_embed, ' ' );
        $str_embed = str_replace( '<', '', $str_embed );

        if ( in_array( $str_embed, $this->_arr_media_types ) ) {

            return $str_embed;
        }

        return false;
    }

}
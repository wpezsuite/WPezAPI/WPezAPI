<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Media;

use WPezSuite\WPezAPI\Get\Attached\ClassAttached;
use WPezSuite\WPezAPI\Get\Embedded\ClassEmbedded;
use WPezSuite\WPezAPI\Get\Featured\ClassFeatured;

class ClassMedia {

    protected $_int_post_id;
    protected $_obj_attached;
    protected $_obj_embedded;
    protected $_obj_featured;

    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_obj_attached = false;
        $this->_obj_embedded = false;
        $this->_obj_featured = false;

    }

    // TODO - Flesh out. Will we need the whole $post?
    public function setPostID( $int ) {

        // TODO - test for not a revision, etc?
        $this->_int_post_id = (integer)$int;

    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'attached':
                return $this->getAttached();

            case 'embedded':
                return $this->getEmbedded();

            case 'featured':
                return $this->getFeatured();

            default:
                return false;
        }

    }

    public function getAttached() {

        if (  $this->_obj_attached === false ) {

            $new        = new ClassAttached();
            $bool =  $new->setPostByID($this->_int_post_id);
            if ( $bool ) {
                $this->_obj_attached = $new;
            } else {
                $this->_obj_attached = '';
            }
        }
        return $this->_obj_attached;
    }

    public function getEmbedded() {

        if (  $this->_obj_embedded === false ) {

            $new        = new ClassEmbedded();
            $bool =  $new->setPostByID($this->_int_post_id);
            if ( $bool ) {
                $this->_obj_embedded = $new;
            } else {
                $this->_obj_embedded = '';
            }
        }
        return $this->_obj_embedded;
    }

    public function getFeatured() {

        if ( ! ( $this->_obj_featured instanceof ClassFeatured ) ) {

            $new        = new ClassFeatured();
            $int_att_id = get_post_thumbnail_id( $this->_int_post_id );
            $new->setPostByID( $int_att_id );
            $this->_obj_featured = $new;
        }
        return $this->_obj_featured;


    }

}

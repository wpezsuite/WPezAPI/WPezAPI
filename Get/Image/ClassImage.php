<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezAPI\Get\Image;

use WPezSuite\WPezAPI\Get\Img\ClassImg;

class ClassImage {

    protected $_int_attachment_id;
    protected $_str_image_size;
    protected $_str_url;
    protected $_str_basename;
    protected $_str_path;
    protected $_int_filesize;
    protected $_arr_filesize_human;
    protected $_arr_attachment_image_src;
    protected $_bool_is_intermediate;
    protected $_obj_img;
    protected $_str_attachment_image;
    protected $_arr_attachment_image_info;
    protected $_arr_meta_size;


    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_int_attachment_id         = false;
        $this->_str_image_size            = 'original';
        $this->_str_url                   = false;
        $this->_str_basename              = false;
        $this->_str_path                  = false;
        $this->_int_filesize              = false;
        $this->_arr_filesize_human        = false;
        $this->_arr_attachment_image_src  = false;
        $this->_bool_is_intermediate      = null;
        $this->_obj_img                   = false;
        $this->_str_attachment_image      = false;
        $this->_arr_attachment_image_info = false;
        $this->_arr_meta_size             = false;

    }


    public function setAttachmentImageID( $mix = false ) {

        if ( ( is_string( $mix ) || is_integer( $mix ) ) && wp_attachment_is( 'image', (integer)$mix ) ) {
            $this->_int_attachment_id = (integer)$mix;

            return true;
        }

        return false;
    }

    public function setSize( $str_size = false ) {

        if ( is_string( $str_size ) ) {

            $this->_str_image_size = trim( (string)$str_size );

            return true;

        }

        return false;
    }


    public function __get( $str_prop = '' ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                return $this->getAttachmentID();

            case 'size':
                return $this->getSize();

            case 'url':
                return $this->getURL();

            case 'basename':
                return $this->getBasename();

            case 'path':
                return $this->getPath();

            case 'filesize':
                return $this->getFilesize();

            case 'filesize_human':
                return $this->getFilesizeHuman();

            case 'intermediate':
            case 'is_intermediate':
                return $this->getIntermediate();

            case 'img':
                return $this->getImg();

            default:
                return false;

        }

    }


    public function getAttachmentID() {

        return $this->_int_attachment_id;
    }

    public function getSize() {

        return $this->_str_image_size;
    }


    public function getURL( $mix_fallback = false ) {

        if ( $this->_str_url === false ) {

            $arr = $this->getAttachmentImageInfo();
            if ( isset( $arr['src'] ) ) {

                $this->_str_url = $arr['src'];

                return $this->_str_url;
            }
        }

        return $mix_fallback;
    }

    public function getBasename() {


        if ( $this->_str_basename === false ) {

            $arr_meta_size = $this->getAttachmentMetadata();
            if ( isset( $arr_meta_size['file'] ) ) {

                $this->_str_basename = $arr_meta_size['file'];

            }
        }

        return $this->_str_basename;

    }

    public function getPath( $mix_fallback = false ) {

        if ( $this->_str_path === false ) {

            $arr = $this->getAttachmentImageInfo();
            if ( isset( $arr['src'] ) ) {

                $arr             = parse_url( $arr['src'] );
                $this->_str_path = untrailingslashit( ABSPATH ) . $arr['path'];
            }
        }

        return $this->_str_path;
    }


    public function getFilesize() {

        if ( $this->_int_filesize !== false ) {
            return $this->_int_filesize;
        }
        $mix_path = $this->getPath();
        if ( $mix_path === false ) {
            return false;
        }
        if ( file_exists( $mix_path ) ) {
            $this->_int_filesize = filesize( $this->_str_path . $this->_str_basename );

            return $this->_int_filesize;
        }

        return false;
    }

    public function getFilesizeHuman() {

        if ( $this->_arr_filesize_human !== false ) {
            return $this->_arr_filesize_human;
        }
        $int_temp = $this->getFilesize();

        $arr_defaults = [
            'B'  => false,
            'GB' => false,
            'KB' => false,
            'MB' => false,
            'TB' => false
        ];

        if ( $int_temp === false ) {
            return $arr_defaults;
        }


        $arr_temp                  = $this->filesizeHuman( $int_temp );
        $this->_arr_filesize_human = array_merge( $arr_defaults, $this->filesizeHuman( $int_temp ) );

        return $this->_arr_filesize_human;
    }


    /**
     * Source: https://secure.php.net/manual/en/function.filesize.php
     *
     * @param $int_bytes
     *
     * @return array
     */
    protected function filesizeHuman( $int_bytes ) {

        $int_bytes  = floatval( $int_bytes );
        $arr_args   = [];
        $arr_args[] = [
            'U' => "TB",
            'V' => pow( 1024, 4 )
        ];
        $arr_args[] = [
            'U' => "GB",
            'V' => pow( 1024, 3 )
        ];

        $arr_args[] = [
            'U' => "MB",
            'V' => pow( 1024, 2 )
        ];
        $arr_args[] = [
            'U' => "KB",
            'V' => 1024
        ];
        $arr_args[] = [
            'U' => "B",
            'V' => 1
        ];

        $arr_ret = [];
        foreach ( $arr_args as $arr ) {
            $arr_ret[ $arr['U'] ] = $int_bytes / $arr['V'];

        }

        return $arr_ret;
    }

    // https://developer.wordpress.org/reference/functions/wp_get_attachment_image_src/
    public function getAttachmentImageSrc() {

        if ( $this->_arr_attachment_image_src === false ) {
            $mix = wp_get_attachment_image_src( $this->getAttachmentID(), $this->getSize() );
        }

        if ( is_array( $mix ) ) {
            $this->_arr_attachment_image_src = $mix;
        }

        return $this->_arr_attachment_image_src;

    }

    public function getIntermediate() {

        if ( $this->_bool_is_intermediate === null ) {
            $mix = $this->getAttachmentImageSrc();

            $this->_bool_is_intermediate = false;
            if ( isset( $mix[3] ) ) {

                $this->_bool_is_intermediate = (boolean)$mix[3];
            }

        }

        return $this->_bool_is_intermediate;
    }


    public function getImg() {

        if ( $this->_obj_img instanceof ClassImg ) {
            return $this->_obj_img;
        }

        $str = $this->getAttachmentImage();
        if ( ! empty( $str ) ) {
            $new = new ClassImg();
            $new->setImgMarkup( $str );
            $this->_obj_img = $new;

        }

        return $this->_obj_img;

    }


    /**
     * wp_get_attachment_image()
     *
     * @return mixed
     */
    public function getAttachmentImage() {

        if ( $this->_str_attachment_image === false ) {

            $this->_str_attachment_image = wp_get_attachment_image( $this->_int_attachment_id, $this->_str_image_size );

        }

        return $this->_str_attachment_image;

    }

    /**
     * getAttachmentImage() parsed into an assoc array
     *
     * @return array|bool
     */
    public function getAttachmentImageInfo() {

        if ( $this->_arr_attachment_image_info === false ) {

            $str = $this->getAttachmentImage();
            if ( ! empty( $str ) ) {
                $str = str_replace( '<img ', '', $str );
                $str = str_replace( '/>', '', $str );
                // parse what's left of the sting into an array of pairs
                $mix = shortcode_parse_atts( $str );
                if ( is_array( $mix ) ) {

                    $this->_arr_attachment_image_info = $mix;
                }
            }
        }

        return $this->_arr_attachment_image_info;
    }

    /**
     * wp_get_attachment_metadata() BUT!! only for the current size -
     * https://codex.wordpress.org/Function_Reference/wp_get_attachment_metadata
     *
     * @return bool
     */
    public function getAttachmentMetadata() {

        if ( $this->_arr_meta_size === false ) {

            $mix_ret = wp_get_attachment_metadata( $this->_int_attachment_id );

            if ( is_array( $mix_ret ) && isset( $mix_ret['sizes'][ $this->_str_image_size ] ) && is_array( $mix_ret['sizes'][ $this->_str_image_size ] ) ) {
                $this->_arr_meta_size = $mix_ret['sizes'][ $this->_str_image_size ];

            }
        }

        return $this->_arr_meta_size;
    }


}
<?php

namespace WPezSuite\WPezAPI\Get\AttachmentImageMeta;

class ClassAttachmentImageMeta {

    protected $_int_aperture;
    protected $_str_credit;
    protected $_str_camera;
    protected $_str_caption;
    protected $_int_created_timestamp;
    protected $_str_copyright;
    protected $_int_focal_length;
    protected $_int_iso;
    protected $_float_shutter_speed;
    protected $_str_title;
    protected $_int_orientation;
    protected $_arr_keywords;

    protected $_arr_float;
    protected $_arr_int;
    protected $_arr_str;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_int_aperture          = false;
        $this->_str_credit            = false;
        $this->_str_camera            = false;
        $this->_str_caption           = false;
        $this->_int_created_timestamp = false;
        $this->_str_copyright         = false;
        $this->_int_focal_length      = false;
        $this->_int_iso               = false;
        $this->_float_shutter_speed   = false;
        $this->_str_title             = false;
        $this->_int_orientation       = false;
        $this->_arr_keywords          = false;

        $this->_arr_float = [ 'shutter_speed' ];
        $this->_arr_int   = [ 'aperture', 'created_timestamp', 'focal_length', 'iso', 'orientation' ];
        $this->_arr_str   = [ 'credit', 'camera', 'caption', 'copyright', 'title' ];
    }

    public function __get( $str_prop = '' ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'aperture':
                return $this->getAperture();

            case 'credit':
                return $this->getCredit();

            case 'camera':
                return $this->getCamera();

            case 'caption':
                return $this->getCaption();

            case 'created_timestamp':
                return $this->getCreatedTimestamp();

            case 'copyright':
                return $this->getCopyright();

            case 'focal_length':
                return $this->getFocalLength();

            case 'iso':
                return $this->getISO();

            case 'shutter_speed':
                return $this->getShutterSpeed();

            case 'title':
                return $this->getTitle();

            case 'orientation':
                return $this->getOrientation();

            case 'keywords':
                return $this->getKeywords();

            default:
                return false;
        }
    }

    /** Use wp_get_attachment_metadata() and pass in the (key) 'image_meta' array
     *
     * @param array $arr
     *
     * @return bool
     */
    public function loaderAll( $arr = [] ) {

        if ( is_array( $arr ) ) {
            foreach ( $arr as $str_prop => $mix_value ) {

                $this->setMaster( $str_prop, $mix_value );

            }

            return true;
        }

        return false;
    }


    protected function setMaster( $str_prop = false, $mix_value = null ) {

        if ( $mix_value === null ) {
            return false;
        }

        if ( in_array( $str_prop, $this->_arr_str ) ) {

            $str_tmp_prop        = '_str_' . trim( $str_prop );
            $this->$str_tmp_prop = (string)$mix_value;

            return true;

        } elseif ( in_array( $str_prop, $this->_arr_int ) ) {

            $str_tmp_prop        = '_int_' . trim( $str_prop );
            $this->$str_tmp_prop = (integer)$mix_value;

            return true;


        } elseif ( in_array( $str_prop, $this->_arr_float ) ) {

            $str_tmp_prop        = '_float_' . trim( $str_prop );
            $this->$str_tmp_prop = (float)$mix_value;

            return true;

        } elseif ( $str_prop === 'keywords' ) {

            $this->_arr_keywords = (array)$mix_value;

            return true;
        }

        return false;

    }

    public function setAperture( $int = 0 ) {

        return $this->setMaster( 'aperture', $int );
    }

    public function setCredit( $str = '' ) {

        return $this->setMaster( 'credit', $str );
    }

    public function setCamera( $str = '' ) {

        return $this->setMaster( 'camera', $str );
    }

    public function setCaption( $str = '' ) {

        return $this->setMaster( 'caption', $str );
    }

    public function setCreatedTimestamp( $int = 0 ) {

        return $this->setMaster( 'created_timestamp', $int );
    }

    public function setCopyright( $str = '' ) {

        return $this->setMaster( 'copyright', $str );
    }

    public function setFocalLength( $int = 0 ) {

        return $this->setMaster( 'focal_length', $int );
    }

    public function setISO( $int = 0 ) {

        return $this->setMaster( 'iso', $int );
    }

    public function setShutterSpeed( $float = 0.0 ) {

        return $this->setMaster( 'shutter_speed', $float );
    }

    public function setTitle( $str = '' ) {

        return $this->setMaster( 'title', $str );
    }

    public function setOrientation( $int = 0 ) {

        return $this->setMaster( 'orientation', $int );
    }

    public function setKeywords( $arr = [] ) {

        return $this->setMaster( 'keywords', $arr );
    }


    public function getAperture( $int_fallback = 0 ) {

        if ( $this->_int_aperture === false ) {
            return $int_fallback;
        }

        return $this->_int_aperture;
    }

    public function getCredit( $str_fallback = '' ) {

        if ( $this->_str_credit === false ) {
            return $str_fallback;
        }

        return $this->_str_credit;
    }

    public function getCamera( $str_fallback = '' ) {

        if ( $this->_str_camera === false ) {
            return $str_fallback;
        }

        return $this->_str_camera;
    }

    public function getCaption( $str_fallback = '' ) {

        if ( $this->_str_caption === false ) {
            return $str_fallback;
        }

        return $this->_str_caption;
    }

    public function getCreatedTimestamp( $int_fallback = 0 ) {

        if ( $this->_int_created_timestamp === false ) {
            return $int_fallback;
        }

        return $this->_int_created_timestamp;
    }


    public function getCopyright( $str_fallback = '' ) {

        if ( $this->_str_copyright === false ) {
            return $str_fallback;
        }

        return $this->_str_copyright;
    }

    public function getFocalLength( $int_fallback = 0 ) {

        if ( $this->_int_focal_length === false ) {
            return $int_fallback;
        }

        return $this->_int_focal_length;
    }

    public function getISO( $int_fallback = 0 ) {

        if ( $this->_int_iso === false ) {
            return $int_fallback;
        }

        return $this->_int_iso;
    }

    public function getShutterSpeed( $float_fallback = 0.0 ) {

        if ( $this->_float_shutter_speed === false ) {
            return $float_fallback;
        }

        return $this->_float_shutter_speed;
    }


    public function getTitle( $str_fallback = '' ) {

        if ( $this->_str_title === false ) {
            return $str_fallback;
        }

        return $this->_str_title;
    }

    public function getOrientation( $int_fallback = 0 ) {

        if ( $this->_int_orientation === false ) {
            return $int_fallback;
        }

        return $this->_int_orientation;
    }

    public function getKeywords( $arr_fallback = [] ) {

        if ( $this->_arr_keywords === false ) {
            return $arr_fallback;
        }

        return $this->_arr_keywords;
    }

}
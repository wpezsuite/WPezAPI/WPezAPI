<?php
// Take the sting out of using WP meta and makes it ez :)


/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Meta;

class ClassMeta {

    protected $_arr_meta;

    public function __construct( $arr = false ) {

        $this->setPropertyDefaults();

        if ( $arr !== false ) {
            $this->setMeta( $arr );
        }
    }


    protected function setPropertyDefaults() {

        $this->_arr_meta = [];
    }


    public function setMeta( $arr ) {

        if ( is_array( $arr ) ) {

            $this->_arr_meta = $arr;

            return true;
        }

        return false;
    }

    public function __get( $str_key ) {

        return $this->getSingle( $str_key );

    }

    /**
     * Returns the entire meta array
     *
     * @return mixed
     */
    public function getAll() {

        return $this->_arr_meta;
    }


    // TODO - returns a bool for a given meta key existing or not.
    public function keyExists( $str_key){

        $str_key = trim($str_key);
        if ( isset($this->_arr_meta[$str_key]) ){
            return true;
        }
        return false;
    }


    public function getSingle( $str_key = false, $mix_fallback = '' ) {

        if ( isset( $this->_arr_meta[ $str_key ] ) && is_array( $this->_arr_meta[ $str_key ] ) ) {

            return maybe_unserialize( $this->_arr_meta[ $str_key ][0] );
        }

        return $mix_fallback;
    }

    public function getMulti( $str_key = false, $mix_fallback = [] ) {

        if ( isset( $this->_arr_meta[ $str_key ] ) && is_array( $this->_arr_meta[ $str_key ] ) ) {

            return array_map( 'maybe_unserialize', $this->_arr_meta[ $str_key ] );
        }

        return $mix_fallback;
    }

    // TODO ACF Repeater - its own class?

    // TODO CMB2 Repeater - its own class?

    // Other repeaters ???

}
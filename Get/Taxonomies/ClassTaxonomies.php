<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Taxonomies;

use WPezSuite\WPezAPI\Get\Taxonomy\ClassTaxonomy;

class ClassTaxonomies {

    protected $_obj_post_id;
    protected $_arr_names;
    protected $_arr_obj_taxs;
    protected $_arr_all;


    protected $_obj_wp_error;


    public function __construct( $obj_post = false ) {

        if ( $obj_post !== false ) {
            $this->setPostObject( $obj_post );
        }
        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_obj_post_id  = false;
        $this->_arr_names    = false;
        $this->_arr_obj_taxs = false;
        $this->_arr_all      = false;
        $this->_obj_wp_error = false;

    }


    // TODO setPostType? << ?
    // TODO - allow for post->ID?
    public function setPostObject( $obj_post = false ) {

        // we're going to do the get_object_taxonomies() a bit prematurely
        // (i.e., there's been no request for those just yet)
        // because we don't want the (unnecessary) bloat of assigning the post object to a property
        // within the object. minor as that might be, let's kept it trim and tight.
        if ( $obj_post instanceof \WP_Post ) {

            $this->_obj_post_id = $obj_post->ID;

            $str_output = 'objects';
            if ( $this->_arr_obj_taxs === false ) {
                $this->_arr_obj_taxs = get_object_taxonomies( $obj_post, $str_output );
            }

            $str_output = 'names';
            if ( $this->_arr_names === false ) {
                $this->_arr_names = get_object_taxonomies( $obj_post, $str_output );
            }
            $obj_post = '';
            // TODO
            $this->_obj_wp_error = false;

            return true;
        }

        return false;
    }

    /**
     * WP taxonomies: "Name should only contain lowercase letters and the
     * underscore character..." Therefore, we can use a magic get (note: because
     * this is not possible with tax terms)
     *
     * @param $str_key
     *
     * @return bool
     */
    public function __get( $str_key ) {

        $str_key = strtolower( $str_key );

        $arr_names = $this->getAll( 'names' );

        // TODO - change to instance of
        if ( in_array( $str_key, $arr_names ) ) {

            $this->getAll();

            if ( $this->_arr_all[ $str_key ] instanceof ClassTaxonomy ) {

                return $this->_arr_all[ $str_key ];

            } else {
                return false;
            }

        } else {
            // We don't need an exists() method, we'll just return false when a ->tax_name doesn't exist
            return false;
        }

    }


    public function getAll( $str_output = false ) {

        // just the names? getAll('names')
        if ( $str_output == 'names' ) {
            return $this->_arr_names;
        }

        if ( is_array( $this->_arr_all ) ) {
            return $this->_arr_all;
        }

        // $arr_all = $this->getObjectTaxonomies();

        if ( is_array( $this->_arr_obj_taxs ) ) {

            foreach ( $this->_arr_obj_taxs as $tax => $obj_std ) {

                // fire up the individual taxonomy objs (which can have many terms, as you'll soon see)
                $new = new ClassTaxonomy();
                $new->setPostID( $this->_obj_post_id );
                $mix = $new->setTaxonomyObject( $obj_std );
                // TODO - is this $mix good?
                $this->_arr_all[ $tax ] = $new;

            }
        } else {
            // TODO - no taxes. Now what?
            $this->_arr_all = [];
        }

        return $this->_arr_all;
    }

    /**
     * A pseudo / proxy for WP's native get_object_taxonomies()
     *
     * @param string $str_output
     *
     * @return mixed
     */
    public function getObjectTaxonomies( $str_output = 'objects' ) {

        if ( $str_output != 'names' ) {

            return $this->_arr_obj_taxs;
        }

        return $this->_arr_names;

    }

}
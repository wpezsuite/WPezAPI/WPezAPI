<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Post;

use WPezSuite\WPezAPI\Get\PostBase\AbstractClassPostBase;
use WPezSuite\WPezAPI\Get\Media\ClassMedia;


class ClassPost extends AbstractClassPostBase {

    use \WPezSuite\WPezAPI\Get\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_arr_post_type_not;
    protected $_str_format;
    protected $_bool_sticky;
    protected $_str_template;
    protected $_bool_has_feat_img;
    protected $_int_feat_img_id;
    protected $_obj_img;


    public function __construct( $mix = false ) {

        $this->setPropertyDefaults();
        $this->setPropertyDefaultsPost();

        if ( $mix !== false ) {
            $this->setPostByID( $mix );
        }

    }


    protected function setPropertyDefaultsPost() {

        // TODO - add set'er
        $this->_arr_post_type_not = [
            'attachment',
            'revision', // TODO - does revision belong here???
            'nav_menu_item',
            'custom_css',
            'customize_changeset'

        ];

        $this->_str_format        = 'false';
        $this->_bool_sticky       = 'false';
        $this->_str_template      = false;
        $this->_bool_has_feat_img = 'false';
        $this->_int_feat_img_id   = 'false';
        $this->_obj_img           = false;

        /*
        $this->_bool_revision = false; // < TODO - set'er
        $this->_int_id        = false;
        $this->_obj_post      = false;

        $this->_str_content_filtered = false;


        $this->_str_permalink  = false;
        $this->_str_shortlink  = false;
        $this->_arr_post_class = false;


        $this->_int_author_id = false;
        $this->_obj_author    = false;

        $this->_int_img_id = false;


        $this->_obj_meta = false;
        $this->_obj_taxs = false;
        */
    }


    protected function setPostCheck( $obj_post ) {

        return ! in_array( $obj_post->post_type, $this->_arr_post_type_not );

    }

    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'content':
                return $this->getContent();

            case 'content_filtered':
                return $this->getContentFiltered();

            case 'excerpt':
                return $this->getExcerpt();

            // ---------

            case 'format':
                return $this->getFormat();

            case 'sticky':
            case 'is_sticky':
                return $this->getSticky();

            case 'template':
                return $this->getTemplate();

            case 'feat_img_id':
            case 'featured_image_id':
            case 'thumbnail_id':
            case 'thumb_id':
            case 'post_thumbnail_id':
                return $this->getFeatImgID();

            case 'has_feat_img':
            case 'has_img':
            case 'has_image':
            case 'has_featured_image':
                return $this->hasFeatImg();

            case 'url':
            case 'link':
            case 'permalink':
                return $this->getURL();

            case 'url_short':
            case 'shortlink':
                return $this->getURLShort();

            case 'class':
            case 'post_class':
                return $this->getClass();

            // https://codex.wordpress.org/Function_Reference/next_post_link
            // https://codex.wordpress.org/Class_Reference/WP_Query#Taxonomy_Parameters
            case 'newer_id':
            case 'next_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<

            case 'older_id':
            case 'prev_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<<

            // --- objects ---

            case 'media':
                return $this->getMedia();

            // only works for pages?
            case 'children':
                return 'TODO';

            default:
                return parent::__get( $str_prop );

        }

    }

    //----------------------

    public function getMedia() {

        $new = new ClassMedia();
        $new->setPostID( $this->_int_id );

        return $new;
    }

    public function getContent( $mix_fallback = false ) {

        return $this->getPostContent( $mix_fallback );
    }

    public function getContentFiltered( $mix_fallback = false ) {

        return $this->getPostContentFiltered( $mix_fallback );
    }


    public function getExcerpt( $mix_fallback = false ) {

        return $this->getPostExcerpt( $mix_fallback );
    }


    // --------------------------------------------

    public function getFormat() {

        if ( $this->_str_format != 'false' ) {
            return $this->_str_format;
        }
        $int_id            = $this->getID();
        $this->_str_format = get_post_format( $int_id );

        return $this->_str_format;
    }


    public function getSticky() {

        if ( $this->_bool_sticky != 'false' ) {
            return $this->_bool_sticky;
        }
        $int_id             = $this->getID();
        $this->_bool_sticky = is_sticky( $int_id );

        return $this->_bool_sticky;
    }


    public function getTemplate() {

        if ( $this->_str_template !== false ) {
            return $this->_str_template;
        }

        $int_id              = $this->getID();
        $this->_str_template = get_page_template_slug( $int_id );

        return $this->_str_template;
    }


    public function getFeatImgID() {

        if ( $this->_int_feat_img_id !== 'false' ) {
            return $this->_int_feat_img_id;
        }

        $this->_int_feat_img_id = false;
        $mix_id                 = get_post_thumbnail_id( $this->_obj_post->ID );
        if ( ! empty ( $mix_id ) ) {
            $this->_int_feat_img_id = absint( $mix_id );
        }

        return $this->_int_feat_img_id;
    }


    public function hasFeatImg() {

        if ( is_bool( $this->_bool_has_feat_img ) ) {
            return $this->_bool_has_feat_img;
        }

        $this->_bool_has_feat_img = true;
        if ( $this->getFeatImgID() === false ) {
            $this->_bool_has_feat_img = false;

        }

        return $this->_bool_has_feat_img;
    }


}